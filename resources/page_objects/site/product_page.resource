*** Settings ***
Documentation    "Product detail" page-object
Library    Browser


*** Variables ***
&{PROD_LOCATORS}    add_button=//button[@data-button-action="add-to-cart"]
...                 blockcart_modal=//*[@id="blockcart-modal"]
...                 customization_txt=//*[@id="field-textField1"]
...                 customization_btn=//*[@name="submitCustomizedData"]
...                 customization_lbl=//h6[@class="customization-message"]/label
...                 quantity_number=//*[@id="quantity_wanted"]
...                 out_of_stock_msg=//*[@id="product-availability"]
...                 wrapper=//*[@id="wrapper"]


*** Keywords ***
Add Product To Cart
    [Documentation]    Add the product to the Cart.
    Click    ${PROD_LOCATORS}[add_button]
    Click    ${PROD_LOCATORS}[blockcart_modal]

Add A Customization Message
    [Arguments]    ${txt}
    [Documentation]    Fill the customization message text area with given parameter.
    Fill Text    ${PROD_LOCATORS}[customization_txt]    ${txt}

Validate Customization
    [Documentation]    VSalidate customization Message.
    Click    ${PROD_LOCATORS}[customization_btn]

Customization Message Should Be
    [Arguments]    ${txt}
    [Documentation]    Checks that the given parameter matches the product's customization message.
    ${txt} =    Replace String    ${txt}    ${\n}    ${SPACE}
    Get Text    ${PROD_LOCATORS}[customization_lbl]    ==    ${txt}

Update Quantity
    [Arguments]    ${quantity}
    [Documentation]    Update the product quantity to the given parameter value.
    Fill Text    ${PROD_LOCATORS}[quantity_number]    ${quantity}
    Click    ${PROD_LOCATORS}[wrapper]

Add Button Is Disabled
    [Documentation]    Asserts that the "add to cart" button is disabled.
    Get Element States    ${PROD_LOCATORS}[add_button]    *=    disabled

Out Of Stock Message Is Displayed
    [Arguments]    ${message}
    [Documentation]    Checks that the out of stock message is displayed.
    Get Text    ${PROD_LOCATORS}[out_of_stock_msg]    *=    ${message}
