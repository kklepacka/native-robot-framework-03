*** Settings ***
Documentation    "Cart" page-object.
Library    Browser


*** Variables ***
&{CART_LOCATORS}    order_btn=//a[text()="Commander"]
...                 wrapper=//*[@id="wrapper"]


*** Keywords ***
Product Should Be In Cart
    [Arguments]    ${product_name}
    [Documentation]    Checks that the specified product is found in the cart.
    Get Element    //a[text()="${product_name}"]

Products Should Be In Cart
    [Arguments]    @{products}
    [Documentation]    Verifies that all products are found in the cart.
    FOR    ${product}    IN    @{products}
        Product Should Be In Cart  ${product}[Product]
    END

Update Product Quantity
    [Arguments]    ${product}    ${quantity}
    [Documentation]    Updates product quantity in the cart.
    Type Text    //*[@name="product-quantity-spin" and contains(@aria-label, "${product}")]    ${quantity}
    Click    ${CART_LOCATORS}[wrapper]
    Get Attribute    //*[@name="product-quantity-spin" and contains(@aria-label, "${product}")]
    ...              value    ==    ${quantity}

Delete From Cart
    [Arguments]    ${product}
    [Documentation]    Removes the given product from the cart.
    ${product_id} =    Get Attribute    //*[@name="product-quantity-spin" and contains(@aria-label, "${product}")]
    ...                data-product-id
    Click    //*[@data-link-action="delete-from-cart" and @data-id-product="${product_id}"]

Click Order Button
    [Documentation]    Clicks on the order button to initiate order placing.
    Click    ${CART_LOCATORS}[order_btn]
